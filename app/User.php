<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Team;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    //******* this function returns teams that current user has created and is owner of them
    public function teams()
    {
        return $this->hasMany(Team::class,'owner_id');
    }

    //******* this function returns teams that current user is member in them
    public function memberedTeams(){
        return $this->belongsToMany('App\Team');
    }

    //******* this function returns users roles
    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    //******* this function has responsibilty to create a team and assigned to current user as owner of it
    //******* this function also set current user as member of newly created team
    public function addTeam($title){
        $team = $this->teams()->create(['title'=>$title]);
        $this->memberedTeams()->attach($team->id);
    }

    //******* this function sets relation between a role and current user. it add a role to current user.
    public function addRole($roleId){
        if($this->roles()->find($roleId) == null && Role::find($roleId) != null)
            $this->roles()->attach($roleId);
    }
}
