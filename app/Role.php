<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    //********* this function return users who have current role
    public function users(){
        return $this->belongsToMany('App\User');
    }
    //********* this function add a user to current role
    public function addUser($userId){
        if($this->users()->find($userId) == null)
            $this->users()->attach($userId);
    }
}
