<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','owner_id'
    ];

    //************** this function returns owner of the current team
    public function owner(){
        return $this->belongsTo(User::class,'owner_id');
    }

    //************** this function returns users that are member in the current team
    public function users(){
        return $this->belongsToMany('App\User');
    }

    //************** this function add a user to current team as member
    public function addMember($userId){
        if($this->users()->find($userId) == null)
            $this->users()->attach($userId);
    }
}
