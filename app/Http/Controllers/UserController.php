<?php

namespace App\Http\Controllers;

use App\Role;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
class UserController extends Controller
{
    private $successStatus = 200;

    /**
     * Create New User
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken(config('app.name'))-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * Login User
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => request('email'),'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken(config('app.name'))-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
     * Return Current user details
     *
     * @return \Illuminate\Http\Response
     */
    public function show(){
        $user = Auth::user();
        return response()->json(['user'=>$user],$this->successStatus);
    }

    /**
     * Return Current user teams details
     *
     * @return \Illuminate\Http\Response
     */
    public function teams(Request $request){
        $user = Auth::user();
        return response()->json(['ownedTeams'=>$user->teams,'memeberedTeams'=>$user->memberedTeams ], $this->successStatus);
    }

    /**
     * add current user to a team
     *
     * @return \Illuminate\Http\Response
     */
    public function joinTeam($id){
        $team = Team::find($id);
        if($team != null)
            $team->addMember(Auth::user()->id);
        else{
            return response()->json(['error'=>'Team does not exist!'], 401);
        }
        return response()->json(['team'=>$team->users], $this->successStatus);
    }

    /**
     * Return Current user roles details
     *
     * @return \Illuminate\Http\Response
     */
    public function roles(Request $request){
        $user = Auth::user();
        return response()->json(['roles'=>$user->roles], $this->successStatus);
    }

    /**
     * Add a role to current user roles, and return all roles for current user
     *
     * @return \Illuminate\Http\Response
     */
    public function addRole(Request $request,$id){
        $role = Role::find($id);
        if($role != null)
            $role->addUser(Auth::user()->id);
        else{
            return response()->json(['error'=>'Role does not exist!'], 401);
        }
        return response()->json(['roles'=>Auth::user()->roles], $this->successStatus);
    }
}
