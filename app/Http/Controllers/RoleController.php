<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class RoleController extends Controller
{
    private $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('users')->get();
        return response()->json(['roles'=>$roles],$this->successStatus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $role = new Role($request->all());
        $role->save();
        return response()->json(['roles'=>Role::with('users')->get()], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::with('users')->find($id);
        if($role == null)
            return response()->json(['error'=>'Role not found'], 404);
        return response()->json(['role'=>$role], $this->successStatus);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $foundRole = Role::find($id);
        if($foundRole != null)
            Role::find($id)->update(['title'=>$request['title']]);
        else{
            return response()->json(['error'=>'Role not found'], 404);
        }
        return response()->json(['role'=>Role::find($id)], $this->successStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $foundRole = Role::find($id);
        if($foundRole != null)
            Role::find($id)->delete();
        else{
            return response()->json(['error'=>'Role not found'], 404);
        }
        return response()->json(['roles'=>Role::with('users')->get()], $this->successStatus);
    }
}
