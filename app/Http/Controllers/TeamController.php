<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class TeamController extends Controller
{
    private $successStatus = 200;

    /**
     * Display a list of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::with('users')->get();
        return response()->json(['teams'=>$teams],$this->successStatus);
    }

    /**
     * Store a newly created resource in storage, and return current user teams
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $user = Auth::user();
        $user->addTeam($request['title']);
        return response()->json(['ownedTeams'=>$user->teams], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = Team::with('users')->find($id);
        if($team == null)
            return response()->json(['error'=>'Team not found'], 404);
        return response()->json(['team'=>$team], $this->successStatus);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $user = Auth::user();
        $foundTeam = $user->teams()->find($id);
        if($foundTeam != null)
            Team::find($id)->update(['title'=>$request['title']]);
        return response()->json(['ownedTeams'=>$user->teams], $this->successStatus);
    }

    /**
     * Remove the specified resource from storage. This service delete the resource if current user is the owner of resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $foundTeam = $user->teams()->find($id);
        if($foundTeam != null)
            Team::find($id)->delete();
        else{
            return response()->json(['error'=>'You are not owner of the team!','ownedTeams'=>$user->teams], $this->successStatus);
        }
        return response()->json(['ownedTeams'=>$user->teams], $this->successStatus);
    }
}
