<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('user/register', 'UserController@register');
Route::post('user/login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user', 'UserController@show');
    Route::get('user/teams', 'UserController@teams');
    Route::post('user/team/join/{team}','UserController@joinTeam');
    Route::get('user/roles', 'UserController@roles');
    Route::post('user/role/add/{role}', 'UserController@addRole');

    Route::get('team/all','TeamController@index');
    Route::post('team/create','TeamController@store');
    Route::get('team/{team}','TeamController@show');
    Route::post('team/delete/{team}','TeamController@destroy');
    Route::post('team/update/{team}','TeamController@update');

    Route::get('role/all','RoleController@index');
    Route::post('role/create','RoleController@store');
    Route::get('role/{role}','RoleController@show');
    Route::post('role/update/{role}','RoleController@update');
    Route::post('role/delete/{role}','RoleController@destroy');
});
